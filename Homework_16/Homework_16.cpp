﻿
#include <iostream>
#include <time.h>

int main()
{
    //инициализируем размерность N массива array
    const int N = 5;

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    //получаем сегодняшнюю дату
    int index = buf.tm_mday;

    //объявляем массив размерности N x N
    int array[N][N];

    //определяем нужную строку в массиве
    int DesiredLine = index % N;

    //заводим переменную для суммы строки
    int sum = 0;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;

            //суммируем элементы нужной строки
            if (DesiredLine == i)
                sum += array[i][j];

            std::cout << '\t' << array[i][j];
        }
        std::cout << '\n';
    }
    std::cout << "Sum of line " << DesiredLine << " equals " << sum;
}

